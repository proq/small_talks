#include <iostream>
#include <string>
#include <vector>

class Widget{
public:
    Widget()
    {
        std::cout << "Widget default constructor" << std::endl;
    }
    Widget(const Widget& widget)
    {
        std::cout << "Widget copy constructor" << std::endl;
    }

    Widget(int, bool)
    {
        std::cout << "Constructor with int and bool" << std::endl;
    }

    Widget(int, double)
    {
        std::cout << "Constructor with int and double" << std::endl;
    }

    Widget(std::initializer_list<std::string>)
    {
        std::cout << "Constructor with initializer list" << std::endl;
    }
};

void playingWithWidget()
{
    Widget w1(10, true);
    Widget w2{10, true};

    Widget w10(10, 10.0);
    Widget w20{10, 10.0};

    Widget({});
    Widget{{}};
}


int main()
{
    Widget widget;
    Widget widget_with_funcy_brackets {};
    Widget widget2 = widget;

    double x = 3.0;
    int y = x;
    int z(x);

    playingWithWidget();

    std::vector <int> vec1(10,2);
    std::vector <int> vec2{10,2};

    for (const auto& v1 : vec1)
    {
        std::cout << v1 << std::endl;
    }
    std::cout << std::endl;

    for (const auto& v2 : vec2)
    {
        std::cout << v2 << std::endl;
    }
    return 0;
}
