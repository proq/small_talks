#include <iostream>

class Singleton
{
private:
    Singleton() = default;
    Singleton(Singleton const&) = delete;
    Singleton& operator=(Singleton const&) = delete;
    
public:

    static Singleton& getInstance()
    {
        static Singleton instance;
        return instance;
    }
    void doSth()
    {
        std::cout << "doSth" << std::endl;
    }
};

int main(int argc, const char * argv[]) {
    Singleton::getInstance().doSth();
    Singleton::getInstance().doSth();
    return 0;
}
