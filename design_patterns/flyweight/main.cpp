#include <iostream>
#include <string_view>
#include <set>
#include <functional>
#include <cstdio>

class device
{
public:
    device(int new_id) : id(new_id){std::cout << "Constructor" << std::endl;}
    bool operator<(const device& dev) const
    {
        return dev.id < this->id;
    }
    ~device(){std::cout << "Destruktor" << std::endl;}

private:
   int id;
};

std::set<device> FlyWeight;

class biggerDevice
{
public:
    biggerDevice(const char* name, int id) : devName(name)
    {
        dev = &*FlyWeight.find(id);
    }
private:
    std::string_view devName;
    const device* dev;
};




int main()
{
    FlyWeight.emplace(5);
    FlyWeight.emplace(5);
    FlyWeight.emplace(6);
    const auto dev = biggerDevice("test", 5);
    std::getchar();
    return 0;
}
