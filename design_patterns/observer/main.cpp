#include <iostream>
#include <vector>
using namespace std;

class Subject
{
    vector <class Observer*> views;
    int value;
public:
    void attach(Observer *object)
    {
        views.push_back(object);
    }
    void setVal(int val)
    {
        value = val;
        notify();
    }
    int getVal()
    {
        return value;
    }
    void notify();
};

class Observer
{
    Subject *model;
    int denom;
public:
    Observer(Subject *_model, int div)
    {
        model = _model;
        denom = div;
        model->attach(this);
    }
    virtual void update() = 0;
protected:
    Subject* getSubject()
    {
        return model;
    }
    int getDivisor()
    {
        return denom;
    }
};

void Subject::notify()
{
    for (const auto& item : views)
        item->update();
}

class DivObserver: public Observer
{
public:
    DivObserver(Subject *mod, int div): Observer(mod, div){}
    
    void update()
    {
        int v = getSubject()->getVal(), d = getDivisor();
        cout << "update: " << v << " div " << d << " is " << v/d << endl;
    }
};

class ModObserver: public Observer
{
public:
    ModObserver(Subject *mod, int div): Observer(mod, div){}
    void update()
    {
        int v = getSubject()->getVal(), d = getDivisor();
        cout << "update: " << v << " mod " << d << " is " << v % d << '\n';
    }
};

int main()
{
    Subject subj;
    DivObserver divObs1(&subj, 4);
    DivObserver divObs2(&subj, 3);
    ModObserver modObs3(&subj, 3);
    subj.setVal(14);
}
